
def reverser(&block)
  block.call.split.map {|word| word.reverse}.join(' ')
end

def adder(adds = 1, &block)
  adds + block.call
end

def repeater(rep = 1, &block)
  rep.times do
    block.call
  end
end
